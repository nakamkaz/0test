package main

import "fmt"

type Vector struct {
	e []float32
}

func (v *Vector) Len() int {
	return len(v.e)
}

func Plus(v1 Vector, v2 Vector) Vector {

	r := Vector{make([]float32, v1.Len())}
	if v1.Len() == v2.Len() {
		for idx, val := range v1.e {
			r.e[idx] = val + v2.e[idx]
		}
	}
	return r
}

func main() {
	v1 := Vector{[]float32{1.0, 2.0, 3.0, 4.0}}
	v2 := Vector{[]float32{1.0, 2.0, 3.0, 4.0}}
	fmt.Println(Plus(v1, v2))
}

// http://goiduuid.appspot.com/p/sSxS7rFA_m