package main
// for readline, ref http://www.yunabe.jp/tips/golang_readlines.html
// for map, ref http://ashitani.jp/golangtips/tips_map.html
// for strings.split ref http://ashitani.jp/golangtips/tips_string.html#string_Split
import (
"fmt"
"bufio"
"os"
"strings"
)

var daom = map[string]string{}

func findItem(name string){
file, err := os.Open("sample.txt")
if err != nil {
return;
}

scanner := bufio.NewScanner(file)
for scanner.Scan() {
	slt := strings.Split(scanner.Text(),",")
	daom[slt[0]] = slt[1]
}
fmt.Println("I am finding ", name, " value")
fmt.Println(daom[name])
}


func main(){
findItem("hoge1");

}
