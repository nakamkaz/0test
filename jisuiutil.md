
## ImageMagick で画像を時計回りに90度回転
```
$ convert -rotate "90" in.jpg out.jpg
```

## ImageMagick で画像を左右半分に割る

```
$ convert in.jpg -crop 54%x100%  +repage out-2d.jpg
```

