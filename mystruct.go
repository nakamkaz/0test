package main
import (
"fmt"
)

type ItemPair struct {
	Value float64
	Price float64
}

type AProp struct {
	Name string
	IPlist []ItemPair
}


func main(){

ip := ItemPair { float64(2100),float64(3.0112) }
fmt.Println(ip)
fmt.Println(ip.Value)
fmt.Println(ip.Price)

x := AProp {
	"Hender",
	[]ItemPair{
	{float64(100),float64(32.00)},
	{float64(102),float64(642.02)},
	{float64(104),float64(302.06)},
	{float64(109),float64(33.08)},
	},
}

fmt.Println(x)
fmt.Println(x.Name)
fmt.Println(x.IPlist[0])

}
