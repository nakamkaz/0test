package main
import "fmt"

func main(){

arr := []float64{1,2,3,4,5,6} 
// An array has [index,value] series
// suppose x for 'index', num for 'value' 
for x, num := range arr {
    fmt.Println(x, num)
}

}

