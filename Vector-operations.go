package main

import "fmt"

type Vector struct {
	e []float32
}

func (v *Vector) Len() int {
	return len(v.e)
}

func Plus(v1 Vector, v2 Vector) Vector {
	r := Vector{make([]float32, v1.Len())}
	if v1.Len() == v2.Len() {
		for idx, val := range v1.e {
			r.e[idx] = val + v2.e[idx]
		}
	} else {
		panic("element num not match")
	}
	return r

}

func Minus(v1 Vector, v2 Vector) Vector {
	r := Vector{make([]float32, v1.Len())}
	if v1.Len() == v2.Len() {
		for idx, val := range v1.e {
			r.e[idx] = val - v2.e[idx]
		}
	} else {
		panic("element num not match")
	}
	return r
}

func InnerProduct(v1 Vector, v2 Vector) float32 {
	var rt float32
	if v1.Len() == v2.Len() {
		for idx, val := range v1.e {
			rt = rt + val*v2.e[idx]
		}
	} else {
		panic("element num not match")
	}
	return rt

}

func main() {
	v1 := Vector{[]float32{1.0, 1.0}}
	v2 := Vector{[]float32{1000.0, -1000.0}}
	fmt.Println(Plus(v1, v2))
	fmt.Println(Minus(v1, v2))
	fmt.Println(InnerProduct(v1, v2))

}
// http://goiduuid.appspot.com/p/35g07Po9U7