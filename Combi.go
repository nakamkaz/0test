package main

import (
	"log"
)

func main() {
	log.Println("3!", factorial(3))
	log.Println("4!", factorial(4))
	log.Println("5!", factorial(5))
	log.Println("0!", factorial(0))
	log.Println("5P3", Permut(5, 3))
	log.Println("R 5P3", PermutR(5, 3))
	log.Println("6P2", Permut(6, 2))
	log.Println("R 6P2", PermutR(6, 2))
	log.Println("5C3", Combi(5, 3))
	log.Println("R 5C3", CombiR(5, 3))

}
func Permut(a, b int) int {

	if a == 0 || b == 0 {
		panic("Combi must take integer that Abs >=1")
	}
	if a < 0 {
		a = -a
	}
	if b < 0 {
		b = -b
	}
	if a <= b {
		panic("Combi must use abs(a)>=abs(b)")
	}
	return int(factorial(a) / factorial(b))
}

func PermutR(a, b int) int {

	if a == 0 || b == 0 {
		panic("Combi must take integer that Abs >=1")
	}
	if a < 0 {
		a = -a
	}
	if b < 0 {
		b = -b
	}
	if a <= b {
		panic("Combi must use abs(a)>=abs(b)")
	}
	for i := a - 1; i > b; i-- {
		a = i * a
	}
	return a
}

func Combi(a, b int) int {
	return int(Permut(a, b) / factorial(a-b))
}

func CombiR(a, b int) int {
	ct := a
	if a == 0 || b == 0 {
		panic("Combi must take integer that Abs >=1")
	}
	if a < 0 {
		a = -a
	}
	if b < 0 {
		b = -b
	}
	if a <= b {
		panic("Combi must use abs(a)>=abs(b)")
	}
	for i := a - 1; i > b; i-- {
		a = i * a
	}
	return int(a / factorial(ct-b))
}

func factorial(a int) int {
	if a >= 1 {
		for i := a - 1; i > 1; i-- {
			a = i * a
		}
	} else if a == 0 {
		a = 1

	} else if a < 0 {
		a = 0
	}
	return a
}
