package main

import (
        "fmt"
        "io/ioutil"
	"io"
        "os"
	"crypto/md5"
	"encoding/hex"
)


func main(){
	cwd,_ := os.Getwd()
	fileinfos, _ := ioutil.ReadDir(cwd)

	for _,fileinfo := range fileinfos {
//		parsedir(fileinfo)
	}
}
/*
func parsedir(finfo io.FileInfo){
	if !finfo.IsDir() {
		fmt.Print(finfo.Name()," ",finfo.Size()," ")
		strmd5,_ := hash_file_md5(finfo.Name())
		fmt.Println(strmd5)
	} else {
          fmt.Println(finfo.Name() ,"is dir")
	}
}
*/
func hash_file_md5(filePath string) (string, error) {
	var returnMD5String string
	file, err := os.Open(filePath)
	if err != nil {
		return returnMD5String, err
	}
	defer file.Close()
	hash := md5.New()
	if _, err := io.Copy(hash, file); err != nil {
		return returnMD5String, err
	}
	hashInBytes := hash.Sum(nil)[:16]
	returnMD5String = hex.EncodeToString(hashInBytes)
	return returnMD5String, nil

}
