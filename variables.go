package main

import (
	"fmt"
)

func main() {
	var a string = "Initial"
	var b, c int = 10, 20
	fmt.Println(a)
	fmt.Println(b, c)

	var d = true
	fmt.Println(d)

	var e int
	fmt.Println(e)

	f := "short"

	fmt.Println(f)
	f = "aaaa"

	fmt.Println(f)

}
