package main

import (
	out "fmt"
)

func main() {
	out.Println("go" + "llnag")
	out.Println("10000+100", 10000+100)
	out.Println("0.7+7.07= ", 0.7+7.07)
	out.Println("7.0/3.0= ", 7.0/3.0)
	out.Println(true && false)
	out.Println(true || false)
	out.Println(!true)
}
