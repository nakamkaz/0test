package main
//  https://gobyexample.com/maps 
import "fmt"

func main(){

	amap := make(map[string]int)

	amap["k1"] = 7
	amap["k2"] = 13

	fmt.Println("map:",amap)

	v1 := amap["k1"]
	fmt.Println("v1: ", v1)

	fmt.Println("length: ", len(amap))
	
	delete (amap, "k2")
	fmt.Println("map:", amap)

	_,prs :=amap["k2"]
	fmt.Println("prs:" , prs)

	json_n := map[string]int{"foo":1, "bar":2}

	fmt.Println("map json_n:", json_n)
	
}



