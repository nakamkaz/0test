package main
import "fmt"
import "math"

func expectedValue(a[]float64) float64{

var ev float64 = 0
        for _,val := range a {
                ev += val
        }
        return ev/(float64)(len(a))
}

func variance(a[]float64) float64 {
    vv := make([]float64,len(a))
        for i,_ := range a {
                vv[i] = a[i] * a[i]
        }
    return expectedValue(vv) - expectedValue(a)*expectedValue(a)
}

func main(){

arr := []float64{1,2,3,4,5,6,7,8,9}
for x, num := range arr {
        fmt.Println(x, num)
}

fmt.Println("ex: ", expectedValue(arr))
fmt.Println("V: ", variance(arr))
fmt.Println("STDDEV: ", math.Sqrt(variance(arr)))
}
