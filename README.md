# README #

[https://gobyexample.com/](https://gobyexample.com/) の実行めも

http://go-tour-jp.appspot.com/

参考
[http://golang.jp/go_tutorial](http://golang.jp/go_tutorial)

My go playground on gae
[http://goiduuid.appspot.com/](http://goiduuid.appspot.com/)

[A Tour of Go の練習問題](http://qiita.com/kroton/items/8622d5e9e38ff822070c)

[Build Web Application with go lang](https://astaxie.gitbooks.io/build-web-application-with-golang/content/ja/)

[atcmaintmpl](https://bitbucket.org/nakamkaz/atcmaintmpl/src/master/)

# 取得
 git clone https://nakamkaz@bitbucket.org/nakamkaz/0test.git

 0test/ができる

# ファイル追加
 git add a1.go
 git add a2.go

# コミット
 git commit 

 # プッシュ
 git push

 git remote addとかよくわからない




 # arrays.go 
http://goiduuid.appspot.com/p/1bs8W4Twhs

# values.go
http://goiduuid.appspot.com/p/9kmYgnZhUt

# variables.go
http://goiduuid.appspot.com/p/et-Enh7J7U

# constraints.go
http://goiduuid.appspot.com/p/T0rXJQWIK-

# functions.go
http://goiduuid.appspot.com/p/VC3i8BQc2r

# gofor.go
http://goiduuid.appspot.com/p/PYuIgecH11

# ifelse.go
http://goiduuid.appspot.com/p/6_tS-RuCpo

# map.go
http://goiduuid.appspot.com/p/NOZy5z7aLM

# multivaluefunc.go
http://goiduuid.appspot.com/p/Hcgarl_Mj6

#  ranges.go
http://goiduuid.appspot.com/p/PxIJyQmCxu

# slices.go
http://goiduuid.appspot.com/p/fY23RiGpJt

# variadicfunc.go
http://goiduuid.appspot.com/p/9VuH1ciHBd

# goaround go
http://goiduuid.appspot.com/p/BcFs-p-Ks0

# Interfaces
http://goiduuid.appspot.com/p/_2wc4nMOPR
####

# Errors  
http://goiduuid.appspot.com/p/WDqnWvn5iA
http://goiduuid.appspot.com/p/_e61KuEJRd

# Goroutine
http://goiduuid.appspot.com/p/W0lO74b900


# Channel
http://goiduuid.appspot.com/p/GW1VJgDkJ6


# twelve_earthly_branches
http://goiduuid.appspot.com/p/9cDyS6z2We
[twelve_earthly_branches](http://goiduuid.appspot.com/p/3oGY-3s2QH)
[2ndEd ]( http://goiduuid.appspot.com/p/nyMS0LzfH1 )

# [Struct/Method http://goiduuid.appspot.com/p/gq5K3y83Fj ](http://goiduuid.appspot.com/p/gq5K3y83Fj)

# type / method 2
http://goiduuid.appspot.com/p/C9oSjVm6ew

# type alias / method Inheritance
http://goiduuid.appspot.com/p/s5Y9GhgUjW

# interface 
http://goiduuid.appspot.com/p/SpS2g4Nr6n

# BlankInterface
http://goiduuid.appspot.com/p/VRIbRMKDdM


# stddev variance E(X) for 6
http://goiduuid.appspot.com/p/nGOA5ulFOg

# cwd files
http://goiduuid.appspot.com/p/PtWVE-EIIQ

# Interface s
http://goiduuid.appspot.com/p/dgv5thDE-Y

# Interfaces
http://goiduuid.appspot.com/p/poG_Nx-imw


https://go-tour-jp.appspot.com/methods/18 の回答
 http://goiduuid.appspot.com/p/d8ZiXJF1mR


# Exercise: Readers
https://go-tour-jp.appspot.com/methods/22
 http://goiduuid.appspot.com/p/BDsisaWvnU

# 2Values struct type and scan 
http://goiduuid.appspot.com/p/P_YflDygTo

# Date time format for JST Asia/Tokyo
http://goiduuid.appspot.com/p/edTi1LNooU

# array and indexing mock
http://goiduuid.appspot.com/p/_eeUAm0JcT

# m array indexing
http://goiduuid.appspot.com/p/4ImludkUHb

# dot product and numpy like object
http://goiduuid.appspot.com/p/AtE6G_6R8l

# string lines to new excise
http://goiduuid.appspot.com/p/qGvykVw7rK

# Struct as slice
http://goiduuid.appspot.com/p/8af84GfcW5

# Key Value struct with array-field
http://goiduuid.appspot.com/p/9SSB_8ZR3f

# Key Value struct with map[string]string
http://goiduuid.appspot.com/p/rbVd9ifhmn

# min a,b / min in int array
http://goiduuid.appspot.com/p/qG3ab7ktwX


