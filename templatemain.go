package main

import (
        "os"
        "text/template"
)

type Model struct {
        Name    string
        Text    string
        Version int
}

func main() {

        vartmpl := "This is {{.}}.\n"
        tpl, errt := template.New("text1").Parse(vartmpl)
        if errt != nil {
                panic(errt)
        }
        errt = tpl.Execute(os.Stdout, "a pen")
        if errt != nil {
                panic(errt)
        }

        t := template.Must(template.ParseFiles("plain.tmpl"))

        errt2 := t.Execute(os.Stdout, Model{
                Name:    "Subject aaaaa iiii",
                Text:    "It is a content",
                Version: 8,
        })
        if errt2 != nil {
                panic(errt2)
        }

}

/*
Makefile 
#-
# git clone git@bitbucket.org:snippets/nakamkaz/8ea7Xj/golang-makefile.git
NAME := jjj
VERSION := 0.1.0

#SRCS := $(shell find . -type f -name '*.go')
SRCS := main.go
LDFLAGS := -ldflags="-s -w -X \"main.Version=$(VERSION)\" -extldflags \"-static\""

default: $(SRCS) fmt
        go build $(LDFLAGS) -o $(NAME) $(SRCS)
fmt:
        go fmt $(SRCS)

*/
